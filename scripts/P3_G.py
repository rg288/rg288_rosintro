# I modified the code that was given by the Moveit interface tutorial. The original code can be found at: https://github.com/ros-planning/moveit_tutorials/blob/master/doc/move_group_python_interface/scripts/move_group_python_interface_tutorial.py 

from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import time

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

# END_SUB_TUTORIAL


def all_close(goal, actual, tolerance):

    if type(goal) is list:
        for index in range(len(goal)):
            if abs(actual[index] - goal[index]) > tolerance:
                return False

    elif type(goal) is geometry_msgs.msg.PoseStamped:
        return all_close(goal.pose, actual.pose, tolerance)

    elif type(goal) is geometry_msgs.msg.Pose:
        x0, y0, z0, qx0, qy0, qz0, qw0 = pose_to_list(actual)
        x1, y1, z1, qx1, qy1, qz1, qw1 = pose_to_list(goal)

        d = dist((x1, y1, z1), (x0, y0, z0))

        cos_phi_half = fabs(qx0 * qx1 + qy0 * qy1 + qz0 * qz1 + qw0 * qw1)
        return d <= tolerance and cos_phi_half >= cos(tolerance / 2.0)

    return True


class MoveGroupPythonInterfaceTutorial(object):
    """MoveGroupPythonInterfaceTutorial"""

    def __init__(self):
        super(MoveGroupPythonInterfaceTutorial, self).__init__()

        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("move_group_python_interface_tutorial", anonymous=True)


        robot = moveit_commander.RobotCommander()

        scene = moveit_commander.PlanningSceneInterface()

        group_name = "manipulator"
        move_group = moveit_commander.MoveGroupCommander(group_name)

        display_trajectory_publisher = rospy.Publisher(
            "/move_group/display_planned_path",
            moveit_msgs.msg.DisplayTrajectory,
            queue_size=20,
        )

        planning_frame = move_group.get_planning_frame()
        print("============ Planning frame: %s" % planning_frame)

        eef_link = move_group.get_end_effector_link()
        print("============ End effector link: %s" % eef_link)

        group_names = robot.get_group_names()

        print("============ Available Planning Groups:", robot.get_group_names())
        print("============ Printing robot state")
        print(robot.get_current_state())
        print("")

        self.box_name = ""
        self.robot = robot
        self.scene = scene
        self.move_group = move_group
        self.display_trajectory_publisher = display_trajectory_publisher
        self.planning_frame = planning_frame
        self.eef_link = eef_link
        self.group_names = group_names

    def go_to_joint_state(self, base, shoulder, elbow, wrist1, wrist2, wrist3):
        move_group = self.move_group

        joint_goal = move_group.get_current_joint_values()
        joint_goal[0] = base        # sets the first join_goal to be UR5e's base joint
        joint_goal[1] = shoulder    # sets the second joint_goal to the UR5e's shoulder joint
        joint_goal[2] = elbow       # sets the third joint_goal to the UR5e's elbow joint
        joint_goal[3] = wrist1      # sets the fourth joint_goal to the UR5e's wrist_1 joint
        joint_goal[4] = wrist2      # sets the fifth joint_goal to the UR5e's wrist_2 joint
        joint_goal[5] = wrist3      # sets the sixth joint_goal to the UR5e's wrist_3 joint

        move_group.go(joint_goal, wait=True)

        move_group.stop() # Stops the robot which prevents any residual movement

        current_joints = move_group.get_current_joint_values()
        return all_close(joint_goal, current_joints, 0.01)


    def go_to_pose_goal(self, x, y, z, roll, pitch, yaw, w):

        move_group = self.move_group

        pose_goal = geometry_msgs.msg.Pose()
        pose_goal.position.x = x                # sets the x position of the pose_goal
        pose_goal.position.y = y                # sets the y position of the pose_goal
        pose_goal.position.z = z                # sets the z position of the pose_goal
        pose_goal.orientation.x = roll          # sets the x orientation of the pose_goal
        pose_goal.orientation.y = pitch         # sets the y orientation of the pose_goal
        pose_goal.orientation.z = yaw           # sets the z orientation of the pose_goal
        pose_goal.orientation.w = w             # sets the w orientation of the pose_goal

        move_group.set_pose_target(pose_goal)

        success = move_group.go(wait=True)

        move_group.stop()

        move_group.clear_pose_targets()

        current_pose = self.move_group.get_current_pose().pose
        return all_close(pose_goal, current_pose, 0.01)

    
def main():
    try:
        tutorial = MoveGroupPythonInterfaceTutorial()
        time.sleep(1)
        tutorial.go_to_joint_state(0, 0, 0, 0, 0, 0)
        time.sleep(2)
        
        tutorial.go_to_joint_state(0, -tau/4, tau/4, 0, tau/4, 0)   # Starting drawing position

        tutorial.go_to_pose_goal(0.6, -0.3, 0.3, 0, 1, 0, 0)        # Position of the top-right of the G

        tutorial.go_to_pose_goal(0.6, 0, 0.3, 0, 1, 0, 0)           # EE moves 0.3 to the right (Top left of G)

        tutorial.go_to_pose_goal(0.3, 0, 0.3, 0, 1, 0, 0)           # EE moves 0.3 downward along the global negative x-axis (Bottom left of G)
        
        tutorial.go_to_pose_goal(0.3, -0.3, 0.3, 0, 1, 0, 0)        # EE moves 0.3 to the right (Bottom right of the G)

        tutorial.go_to_pose_goal(0.35, -0.3, 0.3, 0, 1, 0, 0)       # EE moves 0.05 upwards along hte global positive x-axis (Top right curl of G)

        tutorial.go_to_pose_goal(0.35, -0.25, 0.3, 0, 1, 0, 0)      # EE moves 0.05 left along the global positive y-axis (Top left of the curl of G)

        tutorial.go_to_pose_goal(0.35, -0.25, 0.5, 0, 1, 0, 0)      # EE moves 0.2 upward along the global positive z-axis (G has finished drawing)
        tutorial.go_to_joint_state(0, -tau/4, tau/4, 0, tau/4, 0)   # Reverts back to the resting position

        print("Drawing has been finished!")
    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return


if __name__ == "__main__":
    main()
