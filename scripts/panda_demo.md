# Panda Arm Documentation and Tutorials

This is a document which will detail how to work with a Panda Arm using the ROS environment.

## Expectation and pre installed packages

- All the information and folders to work with the tutorial will be detailed in the following steps. The only information that needs to be pre-installed for this tutorial is the bash file in the ROS Virtual Machine.

## Objectives for this tutorial

The idea of this tutorial is to provide the student with some knowledge and tools in the following areas:

- How does the ROS environment work
- How to work with a pre-built package and how to incorporate it to our workspace
- How does a robotic arm move, and what type of inputs are needed to make it work
- where to look for documentation in case you want to look for additional information

## Getting started

- It is important to note that the way of loading the panda arm into ROS is not unique to this tutorial. The important takeaways from this walkthrough should be how the commands work, and when we need to use them.

### 1- Load the arm into your environment

Goals for this section:

- Load a panda environment to your working (catkin_ws) folder
- Install the packages and necessary extensions
- Understand how ROS knows what packages to use

Let's clone a repository into our working folder that has a panda arm (this one uses the noetic distribution).

- go to your working forder :`cd /home/netid/catkin_ws`
- source your environment first: `source /opt/ros/noetic/setup.sh`
  - the way a "source" command works is that you are letting the ROS environment know what set of variables and packages should be using at one time. This one is for the global environment and should be used before we set a local package that we may use from another repository
- import the repository to our working folder and install its dependencies (detailed info can be found at [working panda arm repository](https://github.com/justagist/panda_simulator))
  - `git clone -b noetic-devel https://github.com/justagist/panda_simulator`
  - `cd panda_simulator`
  - `./build_ws.sh`
- install some packages that will be useful:

  - `sudo apt install ros-$ROS_DISTRO-gazebo-ros-control ros-${ROS_DISTRO}-rospy-message-converter ros-${ROS_DISTRO}-effort-controllers ros-${ROS_DISTRO}-joint-state-controller ros-${ROS_DISTRO}-moveit ros-${ROS_DISTRO}-moveit-commander ros-${ROS_DISTRO}-moveit-visual-tools`

    - the `$ROS_DISTRO` and `${ROS_DISTRO}` instances should be substituted with your working distribution (`noetic` at the time this tutorial was written)

  - `pip3 install future`
  - `cd ../..` (go one level above your devel folder)
    - in here will be the file that will be used to "source" the packages for this repository
  - `source devel/setup.bash`

Small recap:

- so far we have extracted a repository into our catkin environment
- built it and installed its dependencies
- sourced the working directory for the repository

Let's continue by updating some working directories involving some PID values and adding some namespaces to the launch file.

- For the following step, we are going to copy the code from step 6 into the following folder

  - **Modifying gains for hand and arm controllers**

    - Go into step 6 of this [tutorial](https://ros-planning.github.io/moveit_tutorials/doc/gazebo_simulation/gazebo_simulation.html) and replace/copy it into ros_controllers.yaml in the panda_moveit_config/config folder.

  - **Adding a namespace to the controllers**
    ```
    <node name="controller_spawner" pkg="controller_manager" type="spawner" respawn="false" output="screen"
            args="$(arg transmission)_joint_trajectory_controller
            joint_state_controller
            arm_controller
            hand_controller
            --timeout 20" />
    ```
    - This is done because the spawn_model node of gazebo_ros package requires the robot name to be specified as argument. A more detailed explanation can be found [here](https://medium.com/@tahsincankose/custom-manipulator-simulation-in-gazebo-and-motion-planning-with-moveit-c017eef1ea90)

### 2- Ready for launch!

Now that we have set the parameters for the robot and repository, we are ready to see it in Gazebo and Rviz. Execute the following command:

`roslaunch panda_moveit_config demo_gazebo.launch`

Once this is done, Gazebo and Rviz should open simultaneously.

### 3- Working with Python Code

**IMPORTANT**: every time you open a new window (like the one we are going to use for our python code), we need to source into the repository where the dependencies/modules/models are. In this case the one that should be used are:

1. `source /opt/ros/noetic/setup.sh`
2. `source devel/setup.bash`

for every window.

- Please see this tutorial for an in depth explanation and walkthrough of the Panda Arm's [Move Group Python Interface](http://docs.ros.org/en/kinetic/api/moveit_tutorials/html/doc/move_group_python_interface/move_group_python_interface_tutorial.html). The following steps are a summary of the article.

There are three main key things that we need when working with our code: our position/joint information, the way to move using our joints, and a specific location to move to (cartesian paths). Here is how they work:

#### Getting information about our robot

```
# We can get the name of the reference frame for this robot:
planning_frame = group.get_planning_frame()
print "============ Reference frame: %s" % planning_frame

# We can also print the name of the end-effector link for this group:
eef_link = group.get_end_effector_link()
print "============ End effector: %s" % eef_link

# We can get a list of all the groups in the robot:
group_names = robot.get_group_names()
print "============ Robot Groups:", robot.get_group_names()

# Sometimes for debugging it is useful to print the entire state of the
# robot:
print "============ Printing robot state"
print robot.get_current_state()
print ""
```

#### Joint goals

```
# We can get the joint values from the group and adjust some of the values:
joint_goal = group.get_current_joint_values()
joint_goal[0] = 0
joint_goal[1] = -pi/4
joint_goal[2] = 0
joint_goal[3] = -pi/2
joint_goal[4] = 0
joint_goal[5] = pi/3
joint_goal[6] = 0

# The go command can be called with joint values, poses, or without any
# parameters if you have already set the pose or joint target for the group
group.go(joint_goal, wait=True)

# Calling ``stop()`` ensures that there is no residual movement
group.stop()
```

#### Planning to a Pose Goal

Desired pose for the end-effector:

```
#plan a goal pose

pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.4
pose_goal.position.y = 0.1
pose_goal.position.z = 0.4
group.set_pose_target(pose_goal)

# and now we can execute it using

plan = group.go(wait=True)
# Calling `stop()` ensures that there is no residual movement
group.stop()
# It is always good to clear your targets after planning with poses.
# Note: there is no equivalent function for clear_joint_value_targets()
group.clear_pose_targets()
```

#### Cartesian Paths

You can plan a cartesian path directly by specifying a list of waypoints for the end-effector to go through:

```
waypoints = []

wpose = group.get_current_pose().pose
wpose.position.z -= scale * 0.1  # First move up (z)
wpose.position.y += scale * 0.2  # and sideways (y)
waypoints.append(copy.deepcopy(wpose))

wpose.position.x += scale * 0.1  # Second move forward/backwards in (x)
waypoints.append(copy.deepcopy(wpose))

wpose.position.y -= scale * 0.1  # Third move sideways (y)
waypoints.append(copy.deepcopy(wpose))

# We want the Cartesian path to be interpolated at a resolution of 1 cm
# which is why we will specify 0.01 as the eef_step in Cartesian
# translation.  We will disable the jump threshold by setting it to 0.0 disabling:
(plan, fraction) = group.compute_cartesian_path(
                                   waypoints,   # waypoints to follow
                                   0.01,        # eef_step
                                   0.0)         # jump_threshold

# Note: We are just planning, not asking move_group to actually move the robot yet:
return plan, fraction
```

### Possible errors and pitfalls to avoid:

- Once the launch window has been opened and we are ready to use a python script in our code, it is important that we source the package and directory for **EVERY** new window we open:

1. `source /opt/ros/noetic/setup.sh`
2. `source devel/setup.bash`

Otherwise, ROS will not know what packages it should be using.

- Make sure that the relative paths of any urdf objects you are using are correct (that you are in the same folder level that you wrote the script for)

### Useful Links and re ferenced tutorials

- [How to create a launch package](https://medium.com/@tahsincankose/custom-manipulator-simulation-in-gazebo-and-motion-planning-with-moveit-c017eef1ea90)
- [Working panda arm repository](https://github.com/justagist/panda_simulator)
- [Gazebo Simulation integration](https://ros-planning.github.io/moveit_tutorials/doc/gazebo_simulation/gazebo_simulation.html)
- [Custom Manipulator Simulation in Gazebo and Motion Planning with MoveIt!](https://medium.com/@tahsincankose/custom-manipulator-simulation-in-gazebo-and-motion-planning-with-moveit-c017eef1ea90)
- [Move Group Python interface](http://docs.ros.org/en/kinetic/api/moveit_tutorials/html/doc/move_group_python_interface/move_group_python_interface_tutorial.html)
- [Move Group Interface/Python API](http://docs.ros.org/en/indigo/api/pr2_moveit_tutorials/html/planning/scripts/doc/move_group_python_interface_tutorial.html)