rosservice call /clear
rosservice call /kill "turtle2"
rosservice call /kill "turtle1"

rosservice call /spawn 2 2 1.570796 "turtle1"
rosservice call /spawn 5 3 4.712388 "turtle2"
rosservice call /turtle1/set_pen "{r: 255, g: 0, b: 0, width: 3, 'off': 0}"
rosservice call /turtle2/set_pen "{r: 0, g: 0, b: 255, width: 3, 'off': 0}"


rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[4.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0, 0, 0]' '[0, 0, -1.57079633]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[3.8, 0.0, 0.0]' '[0.0, 0.0, -3.1415926]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0.0, 0.0, 0.0]' '[0.0, 0.0, 2.35]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[2.25, 0.0, 0.0]' '[0.0, 0.0, 0.0]'

rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[9.5, 0, 0]' '[0, 0, 9.42477796]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[0.8, 0, 0]' '[0, 0, 0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[-1.6, 0, 0]' '[0, 0, 0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[-1, 0, 0]' '[0, 0, 3.1415926]'

rosservice call /kill "turtle1"
rosservice call /kill "turtle2"

