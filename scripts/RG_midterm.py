# I modified the code that was given by the Moveit interface tutorial. The original code can be found at: https://github.com/ros-planning/moveit_tutorials/blob/master/doc/move_group_python_interface/scripts/move_group_python_interface_tutorial.py 

from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import time
import math as m

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

class MoveGroupPythonInterfaceTutorial(object):
    """MoveGroupPythonInterfaceTutorial"""

    def __init__(self):
        super(MoveGroupPythonInterfaceTutorial, self).__init__()

        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("move_group_python_interface_tutorial", anonymous=True)


        robot = moveit_commander.RobotCommander()

        scene = moveit_commander.PlanningSceneInterface()

        group_name = "manipulator"
        move_group = moveit_commander.MoveGroupCommander(group_name)

        display_trajectory_publisher = rospy.Publisher(
            "/move_group/display_planned_path",
            moveit_msgs.msg.DisplayTrajectory,
            queue_size=20,
        )

        planning_frame = move_group.get_planning_frame()
        print("============ Planning frame: %s" % planning_frame)

        eef_link = move_group.get_end_effector_link()
        print("============ End effector link: %s" % eef_link)

        group_names = robot.get_group_names()

        print("============ Available Planning Groups:", robot.get_group_names())
        print("============ Printing robot state")
        print(robot.get_current_state())
        print("")

        self.box_name = ""
        self.robot = robot
        self.scene = scene
        self.move_group = move_group
        self.display_trajectory_publisher = display_trajectory_publisher
        self.planning_frame = planning_frame
        self.eef_link = eef_link
        self.group_names = group_names

    def go_to_joint_state(self, base, shoulder, elbow, wrist1, wrist2, wrist3):
        move_group = self.move_group

        joint_goal = move_group.get_current_joint_values()
        joint_goal[0] = base        # sets the first joint_goal to be UR5e's base joint
        joint_goal[1] = shoulder    # sets the second joint_goal to the UR5e's shoulder joint
        joint_goal[2] = elbow       # sets the third joint_goal to the UR5e's elbow joint
        joint_goal[3] = wrist1      # sets the fourth joint_goal to the UR5e's wrist_1 joint
        joint_goal[4] = wrist2      # sets the fifth joint_goal to the UR5e's wrist_2 joint
        joint_goal[5] = wrist3      # sets the sixth joint_goal to the UR5e's wrist_3 joint

        move_group.go(joint_goal, wait=True)

        move_group.stop() # Stops the robot which prevents any residual movement

    def go_to_pose_goal(self, x, y, z): # Goes to a certain position in the world 

        move_group = self.move_group

        pose_goal = geometry_msgs.msg.Pose()
        pose_goal.position.x = x                # sets x position of the pose_goal
        pose_goal.position.y = y                # sets y position of the pose_goal
        pose_goal.position.z = z                # sets z position of the pose_goal

        # This sets the orientation of the end effector to rotate by pi/2 on the y-axis
        pose_goal.orientation.x = 0             # sets x orientation of the pose_goal
        pose_goal.orientation.y = 1/m.sqrt(2)   # sets y orientation of the pose_goal
        pose_goal.orientation.z = 0             # sets z orientation of the pose_goal
        pose_goal.orientation.w = 1/m.sqrt(2)   # sets the w orientation of the pose_goal

        move_group.set_pose_target(pose_goal)
        move_group.stop()
        move_group.clear_pose_targets()


    def G_cartesian(self, scale=1):     # The waypoints in cartesian planning follow the global reference frame
        move_group = self.move_group

        waypoints = []      # starts an array that captures needed waypoints

        wpose = move_group.get_current_pose().pose # Gets the current pose of the arm
        wpose.position.y += scale * 0.1            # Move 0.3 along the positive y-axis
        waypoints.append(copy.deepcopy(wpose))     # Adds the modified waypoint to the array

        wpose.position.z -= scale * 0.3            # Move 0.3 along the negative world z-axis
        waypoints.append(copy.deepcopy(wpose))     

        wpose.position.y -= scale * 0.1            # Move 0.1 along the negative y-axis
        waypoints.append(copy.deepcopy(wpose))
        
        wpose.position.z += scale * 0.1            # Move 0.1 along the positive z-axis
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.y += scale * 0.05           # Move 0.05 along the positive y-axis
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.x -= scale * 0.1            # Move 0.1 along the negative z-axis
        waypoints.append(copy.deepcopy(wpose))
        

        (plan, fraction) = move_group.compute_cartesian_path(   # Computes a path to reach each of the waypoints
            waypoints, 0.01, 0.0 
        )  
        return plan, fraction

    def R_cartesian(self, scale=1):     # These waypoints follow the global axis
        move_group = self.move_group

        waypoints = []                  # starts an array of all of the waypoints that will be needed

        wpose = move_group.get_current_pose().pose
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z += 0.3                  # Moves from bottom left of R to top left
        waypoints.append(copy.deepcopy(wpose))

        # This while loop modifies the original arm's pose and creates a bounded semi-circle
        # It decreases the current z-coordinate, if it meets the criteria then it sets the y-coordinate with the semi-circle function
        # At the end of an iteration, it appends the modified waypoint to the array
        while (True):
            wpose.position.z -= .005    # This rate of decrease ensures that many waypoints are generated

            if ((wpose.position.z < .35) | (wpose.position.y > .5)):    # If the waypoint's z-coordinate < .35 or y-coordinate is > .5 then the loop breaks
                break
            wpose.position.y = -scale * m.sqrt(.005625-(wpose.position.z-.425)**2) +.5 # This function creates a semi-circle that uses the z-coordinate
            waypoints.append(copy.deepcopy(wpose))                                     # The radius of the semi-circle is 0.075
                                                                                       # 0.5 is added to ensure that it starts at the correct position

        wpose.position.z = 0.35                     # Sets the z-coordinate to be 0.35
        wpose.position.y = 0.5                      # Sets the y-coordinate to be 0.5
        waypoints.append(copy.deepcopy(wpose))      # This position is the intersection between the semi-circle and the left line of the R
        
        wpose.position.z -= 0.15                    # Decreases the current z-coordinate by 0.15
        wpose.position.y = 0.425                    # Sets the y-coordinate to be 0.425
        waypoints.append(copy.deepcopy(wpose))      # This position is the bottom right of the R initial's diagonal line

        wpose.position.x -= scale * 0.1             # Retracts the arm by 0.1 along the global negative x-axis
        waypoints.append(copy.deepcopy(wpose))

        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0 
        )  
        return plan, fraction
    
    def B_cartesian(self, scale=1): 

        move_group = self.move_group

        waypoints = [] # Starts an empty array that will waypoints as needed

        wpose = move_group.get_current_pose().pose # gets the current pose of the arm
        waypoints.append(copy.deepcopy(wpose))
        
        wpose.position.z += 0.3                    # move the robot arm 0.3 units along the positive world axis
        waypoints.append(copy.deepcopy(wpose))

        # Top Semi-circle while loop (0.35 < z < 0.5)
        # This semi-circle function is very similar to the one in the R cartesian function, but the y bound is changed
        # This is due to the y-coordinate of the starting position being set to 0.1
        while (True):
            wpose.position.z -= .005

            if ((wpose.position.z < .35) & (wpose.position.y < .1)):
                break
            wpose.position.y = -scale * m.sqrt(.005625-(wpose.position.z-0.425)**2) + .1  
            waypoints.append(copy.deepcopy(wpose))

        wpose.position.z = .35
        wpose.position.y = .1
        waypoints.append(copy.deepcopy(wpose))

        # Bottom Semi-circle while loop (0.2 < z < 0.5)
        while (True):               
            wpose.position.z -= .005

            if ((wpose.position.z < .2)):
                break

            wpose.position.y = -scale * m.sqrt(.005625-(wpose.position.z-0.275)**2) + .1  
            waypoints.append(copy.deepcopy(wpose))

        wpose.position.z = .2                   # Sets the z-coordinate to be 0.2
        wpose.position.y = .1                   # Sets the y-coordinate to be 0.1
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.x -= scale * 0.1         # Retracts the arm by 0.1 along the global negative x-axis
        waypoints.append(copy.deepcopy(wpose))

        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0 
        ) 
        return plan, fraction

    # This function executes a cartesian plan that is inputted
    def execute_plan(self, plan):
        move_group = self.move_group
        move_group.execute(plan, wait=True)

def main():
    try:
        tutorial = MoveGroupPythonInterfaceTutorial()
        tutorial.go_to_joint_state(0, 0, 0, 0, 0, 0)               # Sets all joints to their starting position
        time.sleep(1)                                              # Waits 2 seconds

        tutorial.go_to_joint_state(0, -tau/3, tau/3, 0, tau/4, -tau/4)   # Rotates joints and moves to an easier starting position

        tutorial.go_to_pose_goal(0.4, 0.5, 0.2)                     # Getting in position for R
        tutorial.go_to_pose_goal(0.5, 0.5, 0.2)                     # R initial starting position

        R_plan, fraction = tutorial.R_cartesian()                   # Executes cartesian plan for R
        tutorial.execute_plan(R_plan)
        tutorial.go_to_pose_goal(0.4, 0.425, 0.2)                   # Retracts from R drawing position 
        
        tutorial.go_to_joint_state(0, -tau/3, tau/3, 0, tau/4, -tau/4)   # Goes to resting position

        tutorial.go_to_pose_goal(0.4, 0.2, 0.5)                     # Getting in position for G
        tutorial.go_to_pose_goal(0.5, 0.2, 0.5)                     # G initial starting position

        G_plan, fraction = tutorial.G_cartesian()                   # Executes the cartesian plan for G
        tutorial.execute_plan(G_plan)                               
        tutorial.go_to_pose_goal(0.4, 0.25, 0.3)
        
        tutorial.go_to_joint_state(0, -tau/3, tau/3, 0, tau/4, -tau/4)   # Moves back to resting position
        
        tutorial.go_to_pose_goal(0.4, 0.1, 0.2)                     # Getting in position for B
        tutorial.go_to_pose_goal(0.5, 0.1, 0.2)                     # B initial starting position

        B_plan, fraction = tutorial.B_cartesian()                   # Executes cartesian plan for B
        tutorial.execute_plan(B_plan)
        
        tutorial.go_to_joint_state(0, -tau/3, tau/3, 0, tau/4, -tau/4)  # Returns and ends at resting position

    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return

if __name__ == "__main__":
    main()